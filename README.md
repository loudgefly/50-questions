# Intro
Often system administrator job interviews tend to become difficult to prepare, for both candidates and interviewers. 
This is an attempt to use a list of good questions (credit goes to Fossbytes https://fossbytes.com/top-50-linux-system-administrator-interview-questions-answers/) to define a set of topics a pro system administrator should master.

# Questions (and hopefully some good answers)

## What does nslookup do?
It queries for DNS lookups, connecting to DNS servers and sending query strings. It can work in both interactive and non-interactive mode.

## How do you display the top most process utilizing CPU process?


## How to check all open ports on a Linux machine and block the unused ports?

## What is Linux? How is it different from UNIX?

## Explain the boot process of Unix System in details.

## How do you change the permissions? How to create a file that’s read-only property?

## Explain SUDO in detail. What are its disadvantages?

## What is the difference between UDP and TCP?

## Describe the boot order of a Linux machine.

## Design a 3-tier web application.

## Sketch how you would route network traffic from the internet into a few subnets.

## How do you know about virtualization? Is it good to use?

## What are different levels of RAID and what level will you use for a web server and database server?

## List some latest developments in open source technologies.

## Have you ever contributed to an open source project?

## Systems engineer or a systems administrator? Explain?

## List some of the common unethical practices followed by a system professional.

## What is the common size for a swap partition under a Linux system?

## What does a nameless directory represent in a Linux system?

## How to list all files, including hidden ones, in a directory?

## How to add a new system user without login permissions?

## Explain a hardlink. What happens when a hardlink is removed?

## What happens when a sysadmin executes this command: chmod 444 chmod

## How do you determine the private and public IP addresses of a Linux system?

## How do you send a mail attachment using bash?

## Tell me something about the Linux distros used on servers.

## Explain the process to re-install Grub in Linux in the shortest manner.

## What is an A record, an NS record, a PTR record, a CNAME record, an MX record?

## What is a zombie process? State its causes?

## When do we prefer a script over a compiled program?

## How to create a simple master/slave cluster?

## What happens when you delete the source to a symlink?

## How to restrict an IP so that it may not use the FTP Server?

## Explain the directory structure of Linux. What contents go in /usr/local?

## What is git? Explain its structure and working.

## How would you send an automated email to 100 people at 12:00 AM?

## Tell me about ZFS file system.

## How to change the default run level in a Linux system?

## How would you change the kernel parameters in Linux?

## State the differences between SSH and Telnet.

## How would you virtualize a physical Linux machine?

## Tell me about some quirky Linux commands.

## Explain how HTTPS works.

## Do you know about TOR browser? Explain its working.

## How to trigger a forced system check the next time you boot your machine?

## What backup techniques do you prefer?

## Tell me something about SWAP partition.

## Explain Ping of Death attack.

## How do you sniff the contents of an IP packet?

## Which OSI layer is responsible for making sure that the packet reaches its correct destination?
